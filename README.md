# Automated tests

This repository contains automated API tests for the provided test task.

## Pre-requisites

- Node.js 16+

## Running tests locally

#### Clone this repository
```bash
git clone git@gitlab.com:bobb-rob/api-task-playwright.git
```

#### navigate to the project directory
```bash
cd api-task-playwright
```

#### Install dependencies
```bash
npm install
```

#### Run test 
<!-- Describe that the gulp command below will run the test, generate report and open it on the browser -->
To run the tests, run the following command. This command will run the tests, generate a report and open it on the browser.

```bash
gulp test
```

### Generate report
<!-- Describe the use of allure rporting. -->
I integrated Allure reporting to the project to generate a report after running the tests. To generate the report, run the following command after running the tests.

```bash
npm run report
```

 The report will be served in the `/allure-results` directory.

 Alternatively, you can run the following command to open the default playwright report.

 ```bash
 npm run pw-test
 ```

## Running tests in Gitlab CI/CD
The CI/CD pipeline is configured in the `.gitlab-ci.yml` file to run the automated test on GitLab using the playwright Docker runner.

To run the tests in GitLab CI/CD, push the code to the repository and the pipeline will be triggered automatically.


## Authors
| 👤 Name | Github | GitLab | LinkedIn |
|------|--------|---------|----------|
|Robertson Akpan|[@bobb-rob](https://github.com/bobb-rob)|[@bobb-rob](https://gitlab.com/bobb-rob)|[@RobertsonAkpan](https://www.linkedin.com/in/robertsonakpan/)|

## 🤝 Contributing
Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/bobb-rob/api-task-playwright/-/issues).

## Show your support
Give a ⭐️ if you like this project!

## Acknowledgments
- [Playwright](https://playwright.dev/)
- [Allure](https://docs.qameta.io/allure/)
- [Mocha](https://mochajs.org/)
- [Chai](https://www.chaijs.com/)
- [Axios](https://axios-http.com/)
- [Gulp](https://gulpjs.com/)
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [Docker](https://www.docker.com/)
- [Node.js](https://nodejs.org/)
- [NPM](https://www.npmjs.com/)
- [TypeScript](https://www.typescriptlang.org/)
- [Visual Studio Code](https://code.visualstudio.com/)
