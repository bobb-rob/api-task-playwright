const { exec } = require('child_process');
const gulp = require('gulp');

function runTests() {
  return exec('npx playwright test', (error, stdout, stderr) => {
    console.log(stdout);
    console.error(stderr);
    if (error !== null) {
        console.error(`Error: ${error}`);
    }
  });
}

function serveAllureReport() {
  return exec('npx allure serve allure-results --clean', (error, stdout, stderr) => {
      console.log(stdout);
      console.error(stderr);
      if (error !== null) {
          console.error(`Error: ${error}`);
      }
  });
}

exports.test = gulp.series(runTests, serveAllureReport);