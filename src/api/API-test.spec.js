// @ts-check

import { test, expect } from '@playwright/test';
import ApiMethodUtil from "../utils/api_method_util";
const apiHelper = new ApiMethodUtil();

test.describe('@Automated_Confirm Authentication | Create Account With Required Properties', ()=> {
  const path = '/entries';

  test('Authentication & Authorization Category', async () => {
    const response = await apiHelper.GET(path);
    expect(response.status).toBe(200);
    expect(response.statusText).toBe('OK');

    const foundObjects = response.data.entries.filter((entry) => entry.Category === 'Authentication & Authorization');    
    const foundCount = foundObjects.length;
    const expectedCount = 7;    
   
    expect(foundCount).toBe(expectedCount);
    
    foundObjects.forEach((obj) => {
      console.log(obj);
    });
  });
});