import axios from 'axios';

export default class ApiMethodUtil {
    constructor() {
        this.instance = axios.create({
            baseURL: "https://api.publicapis.org",
            headers: {'Content-Type': 'application/json'},
        });
        this.instance.interceptors.request.use((config) => {
            config.ts = Date.now();
            return config
        })
        this.instance.interceptors.response.use(async (response) => {
            response.latency = `${Number(Date.now() - response.config.ts).toFixed()}ms`;
            return response
        })
    }

    GET = async (url, headers, params) => {
        try {
            return await this.instance({
                method: 'get',
                url: url,
                headers,
                params
            });
        } catch (error) {
            return error.response;
        }
    };

    POST = async (url, headers, data) => {
        let response;
        try {
            response = await this.instance({
                method: 'post',
                url,
                data,
                headers,
            });
        } catch (error) {
            response = error.response;
        }       
        return response;
    }
}